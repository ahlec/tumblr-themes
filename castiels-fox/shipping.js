decodeShip = null;
(function () {
	var FANDOM = {},
		MEMBERS = {},
		SHIPS = {};
		
	// UTILITY FUNCTIONS
	function makeFandom(fandomName, tag, link) {
		if (!link) {
			link = null;
		}
		
		FANDOM[tag] = {'name': fandomName, 'tag': tag, 'link': link };
	}
	
	function makeMember(name, wikiLink, fandom) {
		MEMBERS[name] = {'name': name, 'link': wikiLink, 'fandom': fandom };
	}
	
	function makeShip(shipName, memberA, memberB) {
		SHIPS[shipName] = {'name': shipName, 'memberA': memberA, 'memberB': memberB };
	}
	
	//
	decodeShip = function (tag) {
		tag = tag.toLowerCase();
		if (!SHIPS.hasOwnProperty(tag)) {
			return null;
		}
		
		return {
			'name': SHIPS[tag]['name'],
			'memberA': {
				'name': MEMBERS[SHIPS[tag]['memberA']]['name'],
				'link': MEMBERS[SHIPS[tag]['memberA']]['link'],
				'fandom': FANDOM[MEMBERS[SHIPS[tag]['memberA']]['fandom']]
			},
			'memberB': {
				'name': MEMBERS[SHIPS[tag]['memberB']]['name'],
				'link': MEMBERS[SHIPS[tag]['memberB']]['link'],
				'fandom': FANDOM[MEMBERS[SHIPS[tag]['memberB']]['fandom']]
			}
		};
	}
	
	//
	makeFandom("Supernatural", "spn");
	makeFandom("<i>Harry Potter</i>", "hp");
	makeFandom("Homestuck", "homestuck");
	makeFandom("Sherlock BBC", "sherlock bbc");
	makeFandom("<i>Nightrunner</i> Series", "nightrunner", "http://en.wikipedia.org/wiki/The_Nightrunner_Series");
	makeFandom("Mass Effect", "mass effect");
	makeFandom("How to Train Your Dragon", "httyd");
	makeFandom("Rise of the Guardians", "rotg");
	
	makeMember("Dean Winchester", "http://www.supernaturalwiki.com/index.php?title=Dean_Winchester", "spn");
	makeMember("Castiel", "http://www.supernaturalwiki.com/index.php?title=Castiel", "spn");
	makeMember("Harry Potter", "http://harrypotter.wikia.com/wiki/Harry_Potter", "hp");
	makeMember("Ron Weasley", "http://harrypotter.wikia.com/wiki/Ronald_Weasley", "hp");
	makeMember("Rose Lalonde", "http://mspaintadventures.wikia.com/wiki/Rose_Lalonde", "homestuck");
	makeMember("Kanaya Maryam", "http://mspaintadventures.wikia.com/wiki/Kanaya_Maryam", "homestuck");
	makeMember("Sherlock Holmes", "http://www.sherlockology.com/characters/sherlock-holmes", "sherlock bbc");
	makeMember("John Watson", "http://www.sherlockology.com/characters/dr-john-watson", "sherlock bbc");
	makeMember("Seregil í Korit", "http://nightrunner.wikia.com/wiki/Seregil_%C3%AD_Korit_Solun_Meringil_B%C3%B4kthersa", "nightrunner");
	makeMember("Alec í Amasa", "http://nightrunner.wikia.com/wiki/Alec_%C3%AD_Amasa_of_Kerry", "nightrunner");
	makeMember("Commander Shepard (?)", "http://masseffect.wikia.com/wiki/Commander_Shepard", "mass effect");
	makeMember("Ashley Williams", "http://masseffect.wikia.com/wiki/Ashley_Williams", "mass effect");
	makeMember("Dirk Strider", "http://mspaintadventures.wikia.com/wiki/Dirk_Strider", "homestuck");
	makeMember("Jake English", "http://mspaintadventures.wikia.com/wiki/Jake_English", "homestuck");
	makeMember("Hiccup", "http://howtotrainyourdragon.wikia.com/wiki/Hiccup_Horrendous_Haddock_III", "httyd");
	makeMember("Jack Frost", "http://riseoftheguardians.wikia.com/wiki/Jack_Frost", "rotg");
	
	makeShip("destiel", "Dean Winchester", "Castiel");
	makeShip("rarry", "Harry Potter", "Ron Weasley");
	makeShip("rosemary", "Rose Lalonde", "Kanaya Maryam");
	makeShip("johnlock", "Sherlock Holmes", "John Watson");
	makeShip("aregil", "Seregil í Korit", "Alec í Amasa");
	makeShip("mshepley", "Commander Shepard (?)", "Ashley Williams");
	makeShip("dirkjake", "Dirk Strider", "Jake English");
	makeShip("hijack", "Hiccup", "Jack Frost");
	
})();