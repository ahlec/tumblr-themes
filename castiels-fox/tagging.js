decodeTag = null;
(function () {
	var TAGS = [];
	var table = $(document.createElement("table")),
		headerRow = $(document.createElement("tr")),
		content = $("div.content");
	
	table.addClass("tagTable").appendTo(content);
	headerRow.appendTo(table);
	$(document.createElement("th")).text("Tag").appendTo(headerRow);
	$(document.createElement("th")).text("What it is").appendTo(headerRow);
	$(document.createElement("th")).text("Explanation").appendTo(headerRow);
	
	// UTILITY FUNCTIONS
	function makeTag(tag, whatIs, explanation) {
		TAGS.push({"type": "tag", "tag": tag, "whatIs": whatIs, "explanation": (explanation ? explanation : null)});
		/*var row = $(document.createElement("tr")),
			link = $(document.createElement("a"));
			
		row.addClass("tag").appendTo(table);
		$(document.createElement("td")).append(link).appendTo(row);
		link.text("#" + tag).attr("href", "http://ahlec-si.tumblr.com/tagged/" + tag).attr("target", "_blank");
		
		$(document.createElement("td")).html(whatIs).appendTo(row);
		
		if (explanation) {
			$(document.createElement("td")).html(explanation).appendTo(row);
		}*/
	}
	
	function makeSection(sectionTitle) {
		TAGS.push({"type": "section", "title": sectionTitle});
		/*var row = $(document.createElement("tr"));
		
		row.addClass("section").appendTo(table);
		$(document.createElement("td")).attr("colspan", "3").html(sectionTitle).appendTo(row);*/
	}
	
	decodeTag = function(tag) {
		tag = tag.toLowerCase();
		for (var index = 0; index < TAGS.length; ++index) {
			if (TAGS[index]["type"] == "tag" && TAGS[index]["tag"] == tag) {
				return TAGS[index];
			}
		}
		
		return null;
	}
	
	// TAGS
	makeTag("spn", "Supernatural");
	makeTag("acronym", "LGBTQA", "The acronym is just so long...");
	makeTag("daddy", "Parenting", "I want my daughter to call me 'daddy' more than anything else");
	makeTag("my adorable baby", "Vulpix");
	makeTag("my majestic baby", "Ninetales");
	makeTag("pet peevee", "Eeveelutions");
	makeTag("i am cat", "cats");
	makeTag("meow", "kittens");
	makeTag("i am dog", "dogs");
	makeTag("woof", "puppies");
	
	makeTag("b", "Batman (franchise)");
	makeTag("tt", "Teen Titans");
	makeTag("sym", "Saint Young Men");
	makeTag("pmmm", "Puella Magi Madoka Magica");
	makeTag("sm", "Sailor Moon (franchise)");
	makeTag("ygo", "Yu-Gi-Oh! (franchise)");
	
	makeSection("Homestuck");
	makeTag("homestuck", "Homestuck");
	makeTag("john egbert", "John Egbert");
	makeTag("rose lalonde", "Rose Lalonde");
	makeTag("dave strider", "Dave Strider");
	makeTag("harry potter transgender cosplay", "Jade Harley", "<a href=\"http://ahlec-si.tumblr.com/post/66166441836/sagaciouscejai-jesrever-thats-jade-harley#notes\" target=\"_blank\">this post</a>");
	makeTag("jane crocker", "Jane Crocker");
	makeTag("roxy lalonde", "Roxy Lalonde");
	makeTag("dirk strider", "Dirk Strider");
	makeTag("jake english", "Jake English");
	makeTag("karkat vantas", "Karkat Vantas");
	makeTag("sollux captor", "Sollux Captor");
	makeTag("tavros nitram", "Tavros Nitram");
	makeTag("aradia megido", "Aradia Megido");
	makeTag("nepeta leijon", "Nepeta Leijon");
	makeTag("kanaya maryam", "Kanaya Maryam");
	makeTag("terezi pyrope", "Terezi Pyrope");
	makeTag("vriska serket", "Vriska Serket");
	makeTag("equius zahhak", "Equius Zahhak");
	makeTag("gamzee makara", "Gamzee Makara");
	makeTag("eridan ampora", "Eridan Ampora");
	makeTag("feferi peixes", "Feferi Peixes");
	
	$(document).ready(function () {
		var tagNotice = $("#tagNotice");
		if (tagNotice) {
			var tag = decodeTag(tagNotice.attr("data-tag"));
			
			if (tag) {
				var questionMark = $(document.createElement("div")),
					tooltipText = "This is a tag for <b>" + tag["whatIs"] + "</b>." +
						(tag["explanation"] != null ? "<hr />" + tag["explanation"] : "");
				questionMark.addClass("questionMark").appendTo(tagNotice);
				$(document).tooltip({
					items: "div.questionMark",
					content: tooltipText
				});
			}
		}
		else {
			alert("no tag");
		}
	});
})();