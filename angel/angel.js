$(document).ready(function () {
	var body = $(document.body),
		sidebarContainer = $('#sidebarContainer'),
		viewport = sidebarContainer.find('div.viewport'),
		sidebar = sidebarContainer.find('div.sidebar'),
		shadow1 = $('#angelShadow-1'),
		shadow2 = $('#angelShadow-2'),
		shadow3 = $('#angelShadow-3'),
		shadowOffsetDegree = 7, // Number of pixels when considered at one full offset away
		fanficContainer = $('#excerptContainer'),
		fanficExcerpt = $('#fanficExcerpt'),
		fanficLink = $('#fanficLink'),
		fanficPrev = $('#fanficPrev'),
		fanficNext = $('#fanficNext'),
		fanficHistory = [], // Stack
		fanficHistoryIndex = 0,
		fanficTimeout,
		millisecondsPerFanficCharacter = 100;
		
	function showNewSnippet() {
		var fanfic = getRandomFanficSnippet(fanficHistory[fanficHistory.length - 1]);
		fanficHistory.push(fanfic);
		fanficHistoryIndex = fanficHistory.length - 1;
		fanficExcerpt.html(fanfic.excerpt);
		fanficLink.attr('href', fanfic.link).text(fanfic.name);
		
		if (fanficHistoryIndex > 0) {
			fanficPrev.removeClass('disabled');
		}
		clearTimeout(fanficTimeout);
		fanficTimeout = setTimeout(showNewSnippet, millisecondsPerFanficCharacter * fanficExcerpt.text().length); // Will parse away HTML tags
	};
	showNewSnippet();
	fanficContainer.mouseenter(function () {
		if (fanficHistoryIndex === fanficHistory.length - 1) {
			clearTimeout(fanficTimeout);
		}
	}).mouseleave(function () {
		if (fanficHistoryIndex === fanficHistory.length - 1) {
			fanficTimeout = setTimeout(showNewSnippet, millisecondsPerFanficCharacter * fanficExcerpt.text().length);
		}
	}).show();
	fanficPrev.click(function () {
		if (fanficPrev.hasClass('disabled')) {
			return;
		}
		if (fanficHistoryIndex <= 0) {
			fanficHistoryIndex = 0;
			fanficPrev.addClass('disabled');
			return;
		}
		fanficHistoryIndex -= 1;
		
		fanficExcerpt.html(fanficHistory[fanficHistoryIndex].excerpt);
		fanficLink.attr('href', fanficHistory[fanficHistoryIndex].link).text(fanficHistory[fanficHistoryIndex].name);
		
		clearTimeout(fanficTimeout);
		if (fanficHistoryIndex <= 0) {
			fanficPrev.addClass('disabled');
		}
	});
	fanficNext.click(function () {
		if (fanficHistoryIndex >= fanficHistory.length - 1) {
			fanficHistoryIndex = fanficHistory.length - 1;
			showNewSnippet();
			return;
		}
		fanficHistoryIndex += 1;
		
		clearTimeout(fanficTimeout);
		fanficExcerpt.html(fanficHistory[fanficHistoryIndex].excerpt);
		fanficLink.attr('href', fanficHistory[fanficHistoryIndex].link).text(fanficHistory[fanficHistoryIndex].name);
		
		if (fanficHistoryIndex > 0) {
			fanficPrev.removeClass('disabled');
		}
	});
	
	function mouseMoveHandler(e) {
		var windowHorizHalf = $(window).width() / 2,
			windowVertHalf = $(window).height() / 2,
			horizDistancePercent = (e.clientX - windowHorizHalf) / windowHorizHalf,
			vertDistancePercent = (e.clientY - windowVertHalf) / windowVertHalf;
		
		shadow1.css('top', (vertDistancePercent * shadowOffsetDegree) + 'px').css('left', (horizDistancePercent * shadowOffsetDegree) + 'px');
		shadow2.css('top', (vertDistancePercent * shadowOffsetDegree * 4) + 'px').css('left', (horizDistancePercent * shadowOffsetDegree * 4) + 'px');
		shadow3.css('top', (vertDistancePercent * shadowOffsetDegree * 6) + 'px').css('left', (horizDistancePercent * shadowOffsetDegree * 6) + 'px');
	}
		
	$(document).mousemove(mouseMoveHandler).mousemove();
	
	$(window).resize(function () {
		sidebarContainer.height($(window).height());
		sidebar.css('padding-left', viewport.outerWidth());
		body.css('padding-left', sidebarContainer.outerWidth());
	}).resize();
	
	$(window).scroll(function () {
		sidebarContainer.css('left', -$(window).scrollLeft());
	}).scroll();
	
	$('div.top > .main').lettering();
	
	/* STARFALL */
	function issueStarfall() {
		var timeout;
		
		do {
			timeout = Math.random() * 10000;
		} while (timeout < 600);
		setTimeout(summonStar, timeout);
	}
	
	window.summonStar = function summonStar() {
		var star = $(document.createElement('div')),
			startX = viewport.width() - Math.floor(Math.random() * 75),
			startY = Math.floor(Math.random() * 75),
			endX = -Math.floor(Math.random() * 50) - 36,
			endY = startY + Math.floor(Math.random() * 200) + 100;

		if (endX >= startX || endY <= startY) {
			return;
		}
		
		star.addClass('fallingStar').appendTo(viewport).animate(
			{
				path: new $.path.bezier({
						start: { 
							x: startX, 
							y: startY, 
							angle: 10
						},  
						end: { 
							x: endX,
							y: endY,
							angle: -10, 
							length: 0.25
						}
					}, true),
				opacity: 0.3
			},
			{
				duration: 2000,
				easing: 'easeOutExpo',
				complete: function () {
					star.remove();
				},
				step: function (now, fx) {
				}
			}
		);
		
		issueStarfall();
	};
	
	for (var index = 0; index < Math.max(3, Math.floor(Math.random() * 10)); ++index) {
		issueStarfall();
	}
});