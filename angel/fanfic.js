(function () {
	// Define the fanfics I've written that are posted and should be integrated. This is in an easy format, and will be
	// transformed at runtime.
	var fanfics = [
		{
			name: 'Good things come to those who wait',
			link: 'http://archiveofourown.org/works/1778296',
			rating: 'M',
			excerpts: [
				'Beneath his own lips, Hiccup could feel Jackson\'s soft lips spread open in what he knew was a wide, toothy grin. And suddenly, Hiccup found that he believed he would love to see that sight.',
				'"Hayden," Jackson said, just barely above a whisper. And there wasn\'t anger or hurt in his voice; there was just Jackson — a mix of reverence and unswaying happiness with life.',
				'But every word in Hiccup\'s mouth fell short on his tongue at Jackson\'s smile. After everything he had done and the promise of no return, he was still smiling at Hiccup.',
				'Hiccup took a deep breath and continued walking. The knuckles on his hand howled with pain, but he refused to rub at them.',
				'"But all of those things," Fishlegs began. "All of those things are everyone else. How Astrid treats you. How Jackson acts. What Snotlout thinks. I want to know about you. What did you think?"',
				'"A lot of things change. With or without him," Fishlegs said. "Do you think maybe it didn\'t have to be a bad change?"',
				'Hiccup wasn\'t cute. He was badass. And this was the fucking stupidest thing, that he couldn\'t fucking handle this.'
			]
		},
		{
			name: 'Day of Silence',
			link: 'http://archiveofourown.org/works/1475728',
			rating: 'T',
			excerpts: [
				'He wanted so badly to be something special and unique to Jack, and for the briefest of moments, he had felt like he was. Out of the short list of phrases that Jack thought were important, one of them had been \'<em>Good morning, Hiccup</em>\'.',
				'When Jack finally looked up from his hands, confusion still plain about him, he stared right ahead into the wide, toothy smile spread across Henry\'s freckled face, two crooked front teeth visible beneath thin, curved lips.'
			]
		},
		{
			name: 'Stars',
			link: 'http://archiveofourown.org/works/1379656',
			rating: 'G',
			excerpts: [
				'Hiccup smiled. "Really? With all of those beautiful lights out there and the tall buildings, you managed to get distracted by me?"',
				'A second passed, and then Hiccup\'s hands found their way to Jack\'s shoulder; and he was pushing himself up on his toes. Hiccup\'s eyes closed as he pressed his lips to Jack\'s, slowly, awkwardly, afraid but honest.',
				'That made him feel like this was the right place to be for him, that there was no place better for him at this moment.',
				'It was as though the stars that in other places shone in the night sky had fallen to the Earth, and each of them was a person, with a story to tell and a life they lived, and each light spoke of the presence of one of those fallen stars.'
			]
		},
		{
			name: 'Glow',
			link: 'http://archiveofourown.org/works/1185504',
			rating: 'M',
			excerpts: [
			]
		},
		{
			name: 'In heat',
			link: 'http://archiveofourown.org/works/1231030',
			rating: 'E',
			excerpts: [
				'But a wide, toothy, crooked smile spread across the freckled face in front of him, and when Hiccup looked back, his eyes shone with depth and hope.'
			]
		},
		{
			name: 'Walking Home',
			link: 'http://archiveofourown.org/works/1166371',
			rating: 'G',
			excerpts: [
				'No matter how many lifetimes the little viking would live, Jack was going to be there for all of them.',
				'He closed his eyes and tilted his head back, letting the snow fall on his face, soothing his eyes and relaxing his muscles. Hiccup\'s breath came slower, wisps billowing around falling snowflakes.'
			]
		},
		{
			name: 'The Airport',
			link: 'http://archiveofourown.org/works/1159153',
			rating: 'G',
			excerpts: [
				'All of these things showed before Jackson\'s eyes. A whole life of possibilities; an entire future with this boy he\'d never met but felt like he\'d known for so long.',
				'The forms; the waiting; then finally seeing her. Their little girl. And Jackson would be so terrified, afraid that he was going to drop their porcelain angel, until Harry finally had to convince Jackson to just <em>take</em> her.',
				'But the other boy not turning away, instead keeping the conversation surprisingly lively. Jackson would laugh when the boy would get the hiccups, drinking his soda too damned fast. And they would exchange numbers.',
				'And then they were finally together. Not because it was expected, but because Harry had asked him, pulled him close before Jackson had graduated, and asked him to join him, in a new city, at a new school.'
			]
		},
		{
			name: 'The Broken Pieces',
			link: 'http://archiveofourown.org/works/1133219',
			rating: 'E',
			excerpts: [
				'There wasn\'t anything that she could say, because the truth, the "it wasn\'t all your fault, he manipulated you, he abused you" wasn\'t something Hiccup was ready to hear yet.',
				'For the first time in over a year, Hiccup felt the darkness that had taken hold completely dissipate, and he felt <em>human</em>, something that until this point he had assumed he wasn\'t going to feel again.',
				'Four minutes later, Hiccup knocked on the door to 705, which was answered by a shirtless white-haired boy.<br /><br />"I\'m not alright. And I need someone to talk to.\"',
				'Hiccup was caught up in the moment and was lost to the world and the happiness all around him.',
				'Jack was all smiles and crinkles around his eyes as he lifted his own cup to his mouth and drank, and Hiccup forgot about all of the people around him. Right now, there was just the white-haired boy in front of him, happy and smiling and loveable and <em>there</em> for Hiccup and Hiccup had to catch himself because his first thought was to nuzzle into the boy’s neck.',
				'And as the two boys slept that night with the twinkling holiday lights dimly glowing above their heads, the broken, shattered pieces of Hiccup’s soul began to mend, no longer confined to the dark.'
			]
		},
		{
			name: 'Matrices',
			link: 'http://archiveofourown.org/works/1128235',
			rating: 'G',
			excerpts: [
				'And then he was leaning over the small distance between the two, and Jack lips were pressed to Hiccup\'s left cheek, and Hiccup could feel Jack smiling against his face as his cheeks burned the hottest they ever had.',
				'The conversations became less forced and more genuine, and Hiccup found himself relaxing a bit more each time, though seeing Jack walking toward him each morning  caused him those brief moments of nervousness.'
			]
		},
		{
			name: 'Windmanor',
			link: '/post/92807652536/not-really-a-prompt-but-i-would-love-some-more',
			rating: 'E',
			excerpts: [
				'The woman simply nodded her head, before picking up her bags and walking with a swagger towards the door, as though she were a fashion model. She probably was at one point. Or had hopes of being. The slums of Windmanor was the land for those of broken dreams and the rejects.',
				'Hiccup, splattered with blood that was not his own, shaking from head to toe and face red from tears, fell back against the door, sliding to the ground, and wept.'
			]
		},
		{
			name: 'Send me nudes',
			link: '/post/92711223286/hiccup-reached-over-twisting-the-small-knob-just',
			rating: 'M',
			excerpts: [
				'"Jack are you serious?" He asked, turning his head on the pillow to face the white-haired boy resting just next to him, devilish grin on his face as he tried pointlessly to hide his phone.',
				'Hiccup just stared at the boy, eyes lidded from a mixture of exhaustion and unspoken sarcasm, and Jack continued to stare back, the corners of his mouth pulling tighter almost unnoticeably.'
			]
		},
		{
			name: 'Soaring',
			link: '/post/89101031861/jack-hiccup-cried-out-feebly-his-voice-didnt',
			rating: 'M',
			excerpts: [
				'"You’re not—" His own breaths were coming fast, and he could feel himself hyperventilating. "You’re not going to die.',
				'His left hand still rested on the neck of his slain dragon, but Hiccup raised his right arm, shaking with effort. Dumbly, unsure what to do, Jack grabbed it, holding tightly to it, squeezing it painfully; he couldn’t let go.'
			]
		},
		{
			name: 'He knows',
			link: '/post/86258534576/stuffing-the-binder-into-his-armpit-hiccup-gave-a',
			rating: 'T',
			excerpts: [
				'Everything sped up in Hiccup\'s mind as his stomach fell further south.<br /><br />\'<em>Oh shit he does kno— fuck what am I— why is his voice so deep how do I keep forgetting it\'s— why are so many people here—</em>\'',
				'As quietly as he could muster, Hiccup squeaked (what had been intended as a whisper): "Jack, please not here."',
				'"B-but, it\'s just— it\'s a phase. And I haven\'t gotten a lot of sleep for a very long time, so it\'s probably that. And I don\'t know wha— but I haven\'t been staring at you if that\'s what you\'re accusing. And I\'m not making any moves on you, and I promise I\'m not stalki—" \'<em>Shit I have made this so much worse</em>\''
			]
		},
		{
			name: 'Gone',
			link: '/post/83799266678/it-had-been-over-seven-months-since-henry-hadnt',
			rating: 'T',
			excerpts: [
				'But when the thirtieth day passed and the search mission was called off and Henry declared officially missing (presumed dead), Jack gave up hope.',
				'But more importantly, they had recovered Henry. And Jack was to come and retrieve him.',
				'The receptionist — Macey — looked up, double taking when she saw Jack. He was tired of pity; he could\'t stand the pity any more.'
			]
		},
		{
			name: 'Promises',
			link: '/post/83600721836/hiccups-back-was-pressed-against-the-legs-of-the',
			rating: 'G',
			excerpts: [
				'"And I’ve made a horrible mistake getting this attached to you, Jack. Because no matter how much you promise me that you love me, I know that you’re going to leave me too. And I’ll be alone again in my pain, because I was foolish enough to fall for you."',
				'Hiccup\'s back was pressed against the legs of the sofa, body tucked in on itself as he attempted to make himself as small as he could. He wanted to disappear.',
				'And when Hiccup awoke, groggy and hair a mess from the nightmares that tossed him in his sleep, he found Jack curled up next to him. And eternity didn\'t matter at the moment; for now, that was good enough.'
			]
		}
	];
	
	// Don't edit below this line
	var snippets = [];
	for (var index = 0; index < fanfics.length; index += 1) {
		for (var innerIndex = 0; innerIndex < fanfics[index].excerpts.length; innerIndex += 1) {
			snippets.push({
				name: fanfics[index].name,
				link: fanfics[index].link,
				rating: fanfics[index].rating,
				excerpt: fanfics[index].excerpts[innerIndex]
			});
		}
	}
		
	window.getRandomFanficSnippet = function (previous) {
		var randomSnippet;
		do
		{
			randomSnippet = snippets[Math.floor(Math.random() * snippets.length)];
		} while (previous && previous.name === randomSnippet.name); // don't show another excerpt from the previous fanfic
		return randomSnippet;
	};
}());